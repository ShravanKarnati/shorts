const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");

module.exports = {
  entry: {
    index: ["./src/js/index.js", "./src/scss/index.scss"],
    pig: ["./src/js/pig.js", "./src/scss/pig.scss"],
    hangman: [
      "./src/js/hangman.js",
      "./src/words_hangman.js",
      "./src/scss/hangman.scss",
    ],
    natours: ["./src/scss/natours.scss"],
    nexter: ["./src/scss/nexter.scss"],
    trillo: ["./src/scss/trillo.scss"],
    tictactoe: ["./src/js/tictactoe.js", "./src/scss/tictactoe.scss"],
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        use: ["html-loader"],
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/",
            },
          },
        ],
      },
      {
        test: /\.(jpg|jpeg|png|gif|icon|svg|mp4|webm)$/,
        use: {
          loader: "file-loader",
          options: {
            esModule: false,
            name: "[name].[ext]",
            outputPath: "./img/",
          },
        },
      },
    ],
  },
  plugins: [
    new FaviconsWebpackPlugin("./src/html/favicon.ico"),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "src/html/index.html",
      chunks: ["index"],
    }),
    new HtmlWebpackPlugin({
      filename: "pig.html",
      template: "src/html/pig.html",
      chunks: ["pig"],
    }),
    new HtmlWebpackPlugin({
      filename: "hangman.html",
      template: "src/html/hangman.html",
      chunks: ["hangman"],
    }),
    new HtmlWebpackPlugin({
      filename: "natours.html",
      template: "src/html/natours.html",
      chunks: ["natours"],
    }),
    new HtmlWebpackPlugin({
      filename: "nexter.html",
      template: "src/html/nexter.html",
      chunks: ["nexter"],
    }),
    new HtmlWebpackPlugin({
      filename: "tictactoe.html",
      template: "src/html/tictactoe.html",
      chunks: ["tictactoe"],
    }),
    new HtmlWebpackPlugin({
      filename: "trillo.html",
      template: "src/html/trillo.html",
      chunks: ["trillo"],
    }),
    // new HtmlWebpackPlugin({
    //   favicon: "./src/images/Shorts_Logo.png",
    // }),
  ],
};
