import "../images/1.png";
import "../images/2.png";
import "../images/3.png";
import "../images/4.png";
import "../images/5.png";
import "../images/6.png";

let totalScore_player1 = document.getElementsByClassName(
  "playerscore--player1--score"
)[0];
let totalScore_player2 = document.getElementsByClassName(
  "playerscore--player2--score"
)[0];

let currentScore_player1 = document.getElementsByClassName(
  "currentScore--score--player1"
)[0];
let currentScore_player2 = document.getElementsByClassName(
  "currentScore--score--player2"
)[0];

let rollDice = document.getElementsByClassName("rolldice--text")[0];
let hold = document.getElementsByClassName("hold--text")[0];
let newGame = document.getElementsByClassName("newgame--text")[0];

let indicator_player1 = document.getElementsByClassName(
  "playerslate--player1--indicator"
)[0];
let indicator_player2 = document.getElementsByClassName(
  "playerslate--player2--indicator"
)[0];

let finalScore = document.getElementsByClassName("finalScore--container")[0];

let dice1 = document.getElementsByClassName("dice-1")[0];
let dice2 = document.getElementsByClassName("dice-2")[0];

let currentPlayer = true;
var totalScore = 50;

function randomNum() {
  let randNum = Math.floor(Math.random() * 6 + 1);
  return randNum;
}

function indicator(flag) {
  if (flag) {
    indicator_player1.classList.remove("disp");
    indicator_player2.classList.add("disp");
  } else {
    indicator_player2.classList.remove("disp");
    indicator_player1.classList.add("disp");
  }
}

function changePlayer() {
  currentPlayer = !currentPlayer;
  indicator(currentPlayer);
  return;
}

function checkRules(num1, num2) {
  if ((num1 === num2) === 1) {
    return 2;
  } else if (num1 === 1 || num2 === 1) {
    return 1;
  }
  return false;
}

function checkFinalScore(score) {
  if (score >= totalScore) {
    return true;
  }
  return false;
}

function changeDice(num1, num2) {
  let d1 = "./img/" + num1.toString() + ".png";
  let d2 = "./img/" + num2.toString() + ".png";
  dice1.style.background = "url(" + d1 + ")";
  dice1.style.backgroundPosition = "center";
  dice1.style.backgroundSize = "cover";
  dice2.style.background = "url(" + d2 + ")";
  dice2.style.backgroundPosition = "center";
  dice2.style.backgroundSize = "cover";
}

rollDice.addEventListener("click", function () {
  var scoreTemp1 = randomNum();
  var scoreTemp2 = randomNum();
  changeDice(scoreTemp1, scoreTemp2);
  game(currentPlayer, scoreTemp1, scoreTemp2);
});

hold.addEventListener("click", function () {
  if (currentPlayer) {
    currentScore =
      parseInt(currentScore_player1.textContent) +
      parseInt(totalScore_player1.textContent);
    totalScore_player1.textContent = currentScore;
    currentScore_player1.textContent = "0";
    var temp = parseInt(totalScore_player1.textContent);
    if (checkFinalScore(temp)) {
      if (!alert("Player 1 Won!")) {
        window.location.reload();
      }
    }
  } else {
    currentScore =
      parseInt(currentScore_player2.textContent) +
      parseInt(totalScore_player2.textContent);
    totalScore_player2.textContent = currentScore;
    currentScore_player2.textContent = "0";
    var temp = parseInt(totalScore_player2.textContent);
    if (checkFinalScore(temp)) {
      if (!alert("Player 2 Won!")) {
        window.location.reload();
      }
    }
  }
  changePlayer();
});

newGame.addEventListener("click", function () {
  location.reload();
});

finalScore.addEventListener("keyup", function (event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    if (parseInt(finalScore.value) > 0) {
      totalScore = parseInt(finalScore.value);
      finalScore.value = "";
      placeholderString = "final score = " + totalScore.toString();
      finalScore.placeholder = placeholderString;
    } else {
      totalScore = 50;
    }
  }
});

function game(currentPlayer, scoreTemp1, scoreTemp2) {
  var check = checkRules(scoreTemp1, scoreTemp2);
  if (check === false) {
    var currentScore = scoreTemp1 + scoreTemp2;
    if (currentPlayer) {
      var prevScore = parseInt(currentScore_player1.textContent);
      var currentScore = prevScore + currentScore;
      currentScore_player1.textContent = currentScore;
    } else {
      var prevScore = parseInt(currentScore_player2.textContent);
      var currentScore = prevScore + currentScore;
      currentScore_player2.textContent = currentScore;
    }
  } else if (check === 1) {
    if (currentPlayer) {
      currentScore_player1.textContent = "0";
    } else {
      currentScore_player2.textContent = "0";
    }
    changePlayer();
  } else {
    if (currentPlayer) {
      totalScore_player1.textContent = "0";
    } else {
      totalScore_player2.textContent = "0";
    }
    changePlayer();
  }
}
